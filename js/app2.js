  // Tu configuración de Firebase
  const firebaseConfig = {
    apiKey: "AIzaSyAtSk8KH3qJyNRoU4zbiJ6oLUzHvWS-R68",
    authDomain: "administradorweb-3f3d4.firebaseapp.com",
    projectId: "administradorweb-3f3d4",
    storageBucket: "administradorweb-3f3d4.appspot.com",
    messagingSenderId: "966063272755",
    appId: "1:966063272755:web:92e701c8e1e0f767898446"
  };

firebase.initializeApp(firebaseConfig);

  const storage = firebase.storage();
  const storageRef = storage.ref();

  // Función para subir imagen a Firebase Storage
  function subirImagen() {
    const fileInput = document.getElementById('fileInput');
    const archivo = fileInput.files[0];
    const urlInput = document.getElementById('urlInput');

    if (archivo) {
        // Verificar que el archivo sea una imagen .jpg, .jpeg o .png
        if (archivo.type === 'image/jpeg' || archivo.type === 'image/png') {
            const imagenRef = storageRef.child(`imagenes/${archivo.name}`);

            // Subir imagen a Firebase Storage
            imagenRef.put(archivo).then(() => {
                alert('Imagen subida con éxito.');

                // Mostrar la imagen subida
                imagenRef.getDownloadURL().then((url) => {
                    const imagenContainer = document.getElementById('imagenContainer');
                    const imagen = document.createElement('img');
                    imagen.style.width = '50px'; 
                    imagen.src = url;
                    imagenContainer.appendChild(imagen);

                    // Mostrar URL de la imagen
                    urlInput.value = url;
                }).catch((error) => {
                    alert('Error al obtener la URL de descarga: ' + error.message);
                });
            }).catch((error) => {
                alert('Error al subir la imagen: ' + error.message);
            });
        } else {
            alert('Por favor, selecciona una imagen en formato .jpg, .jpeg o .png.');
        }
    } else {
        alert('Por favor, selecciona un archivo.');
    }
}