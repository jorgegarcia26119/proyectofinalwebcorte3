// Tu configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyAtSk8KH3qJyNRoU4zbiJ6oLUzHvWS-R68",
    authDomain: "administradorweb-3f3d4.firebaseapp.com",
    databaseURL: "https://administradorweb-3f3d4-default-rtdb.firebaseio.com",
    projectId: "administradorweb-3f3d4",
    storageBucket: "administradorweb-3f3d4.appspot.com",
    messagingSenderId: "966063272755",
    appId: "1:966063272755:web:92e701c8e1e0f767898446"
  };
  

// Inicializar Firebase
firebase.initializeApp(firebaseConfig);

// Referencia a la ubicación en la base de datos donde se guardarán los productos
const productosRef = firebase.database().ref('productos');

// Obtener referencia al formulario
const productForm = document.getElementById('productForm');

// Manejar el envío del formulario
productForm.addEventListener('submit', (e) => {
    e.preventDefault();

    // Obtener los valores del formulario
    const codigo = document.getElementById('codigo').value;
    const nombre = document.getElementById('nombre').value;
    const precio = document.getElementById('precio').value;
    const status = document.getElementById('status').value;
    const url = document.getElementById('url').value;

    // Verificar que los campos no estén vacíos
    if (codigo && nombre && precio && status && url) {
        // Crear un objeto con los datos del producto
        const producto = {
            codigo: codigo,
            nombre: nombre,
            precio: precio,
            status: status,
            url: url
        };

        // Agregar el producto a la base de datos
        productosRef.push(producto)
            .then(() => {
                alert('Producto agregado correctamente.');
                // Limpiar el formulario después de agregar el producto
                productForm.reset();
            })
            .catch((error) => {
                alert('Error al agregar el producto: ' + error.message);
            });
    } else {
        alert('Por favor, completa todos los campos.');
    }
});


