// Importa el módulo de autenticación de Firebase
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

// Tu configuración de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyAtSk8KH3qJyNRoU4zbiJ6oLUzHvWS-R68",
  authDomain: "administradorweb-3f3d4.firebaseapp.com",
  databaseURL: "https://administradorweb-3f3d4-default-rtdb.firebaseio.com",
  projectId: "administradorweb-3f3d4",
  storageBucket: "administradorweb-3f3d4.appspot.com",
  messagingSenderId: "966063272755",
  appId: "1:966063272755:web:92e701c8e1e0f767898446"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Obtiene la referencia al elemento del formulario
const btnEnviar = document.getElementById('btnEnviar');

// Agrega un evento de escucha para el envío del formulario
btnEnviar.addEventListener("click", function(event) {
  event.preventDefault(); // Evita que el formulario se envíe de forma convencional

  // Obtiene los valores del correo electrónico y la contraseña desde el formulario
  const email = document.getElementById('txtEmail').value;
  const password = document.getElementById('txtPassword').value;

  // Obtiene la instancia de autenticación de Firebase
  const auth = getAuth();

  // Intenta autenticar al usuario con el correo electrónico y la contraseña proporcionados
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Usuario autenticado correctamente
      const user = userCredential.user;
      alert(`Usuario autenticado${user.email}`);

      // Redirige al usuario a la página de inicio, por ejemplo:
      window.location.href = "/html/inicioAdmin.html";
    })
    .catch((error) => {
      // Error durante la autenticación
      const errorCode = error.code;
      const errorMessage = error.message;
      alert(`Error durante la autenticación: ${errorMessage}`);
   });
});
