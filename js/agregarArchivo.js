
// Tu configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyAtSk8KH3qJyNRoU4zbiJ6oLUzHvWS-R68",
    authDomain: "administradorweb-3f3d4.firebaseapp.com",
    databaseURL: "https://administradorweb-3f3d4-default-rtdb.firebaseio.com",
    projectId: "administradorweb-3f3d4",
    storageBucket: "administradorweb-3f3d4.appspot.com",
    messagingSenderId: "966063272755",
    appId: "1:966063272755:web:92e701c8e1e0f767898446"
  };
  

firebase.initializeApp(firebaseConfig);
  
// Referencia a la ubicación en la base de datos donde se guardarán los productos
const productosRef = firebase.database().ref('productos');
const storage = firebase.storage();
const storageRef = storage.ref();

// Función para subir imagen a Firebase Storage
function subirImagen() {
  const fileInput = document.getElementById('fileInput');
  const archivo = fileInput.files[0];
  const urlInput = document.getElementById('url');

  if (archivo) {
    // Verificar que el archivo sea una imagen .jpg, .jpeg o .png
    if (archivo.type === 'image/jpeg' || archivo.type === 'image/png') {
      const imagenRef = storageRef.child(`imagenes/${archivo.name}`);

      // Subir imagen a Firebase Storage
      imagenRef.put(archivo).then(() => {
        alert('Imagen subida con éxito.');

        // Mostrar la imagen subida
        imagenRef.getDownloadURL().then((url) => {
          const imagenContainer = document.getElementById('imagenContainer');
          const imagen = document.createElement('img');
          imagen.src = url;
          imagenContainer.appendChild(imagen);

          // Mostrar URL de la imagen
          urlInput.value = url;
        }).catch((error) => {
          alert('Error al obtener la URL de descarga: ' + error.message);
        });
      }).catch((error) => {
        alert('Error al subir la imagen: ' + error.message);
      });
    } else {
      alert('Por favor, selecciona una imagen en formato .jpg, .jpeg o .png.');
    }
  } else {
    alert('Por favor, selecciona un archivo.');
  }
}

document.addEventListener("DOMContentLoaded", function() {
  const productForm = document.getElementById('productForm');

  // Manejar el envío del formulario
  productForm.addEventListener('submit', (e) => {
      e.preventDefault();

      // Obtener los valores del formulario
      const codigo = document.getElementById('codigo').value;
      const nombre = document.getElementById('nombre').value;
      const precio = document.getElementById('precio').value;
      const status = document.getElementById('status').value;
      const url = document.getElementById('url').value;

      // Verificar que los campos no estén vacíos
      if (codigo && nombre && precio && status && url) {
          // Verificar si ya existe un producto con el mismo código y status no deshabilitado
          productosRef.orderByChild('codigo').equalTo(codigo).once('value')
              .then((snapshot) => {
                  if (snapshot.exists()) {
                      const productoKeys = Object.keys(snapshot.val());
                      const producto = snapshot.val()[productoKeys[0]];

                      // Verificar si el producto tiene status no deshabilitado
                      if (producto.status !== 'Deshabilitado') {
                          alert('Ya existe un producto con este código y no está deshabilitado.');
                      } else {
                          // Si el producto está deshabilitado, actualizarlo con los nuevos datos
                          const productoKey = productoKeys[0];
                          productosRef.child(productoKey).update({
                              nombre: nombre,
                              precio: parseFloat(precio),
                              status: status,
                              url: url
                          }).then(() => {
                              alert('Producto actualizado correctamente.');
                              // Limpiar el formulario después de agregar o actualizar el producto
                              productForm.reset();
                          }).catch((error) => {
                              alert('Error al actualizar el producto: ' + error.message);
                          });
                      }
                  } else {
                      // Si no existe un producto con el mismo código, agregarlo a la base de datos
                      const producto = {
                          codigo: codigo,
                          nombre: nombre,
                          precio: parseFloat(precio),
                          status: status,
                          url: url,
                      };

                      productosRef.push(producto)
                          .then(() => {
                              alert('Producto agregado correctamente.');
                              // Limpiar el formulario después de agregar o actualizar el producto
                              productForm.reset();
                          })
                          .catch((error) => {
                              alert('Error al agregar el producto: ' + error.message);
                          });
                  }
              })
              .catch((error) => {
                  alert('Error al verificar el código del producto: ' + error.message);
              });
      } else {
          alert('Por favor, completa todos los campos.');
      }
  });
});

// ...
document.addEventListener("DOMContentLoaded", function() {

});

// Función para buscar un producto por su código
function buscarProducto() {
const codigo = prompt('Ingrese el código del producto:');
if (codigo) {
  productosRef.orderByChild('codigo').equalTo(codigo).once('value')
    .then((snapshot) => {
      if (snapshot.exists()) {
        const productoKey = Object.keys(snapshot.val())[0];
        const producto = snapshot.val()[productoKey];

        // Mostrar los detalles del producto en los inputs
        document.getElementById('codigo').value = producto.codigo;
        document.getElementById('nombre').value = producto.nombre;
        document.getElementById('precio').value = producto.precio;
        document.getElementById('status').value = producto.status;
        document.getElementById('url').value = producto.url;
      } else {
        alert('Producto no encontrado.');
      }
    })
    .catch((error) => {
      console.error('Error al buscar el producto:', error);
    });
}
}


function deshabilitarProducto() {
const codigo = prompt('Ingrese el código del producto que desea deshabilitar:');
if (codigo) {
  productosRef.orderByChild('codigo').equalTo(codigo).once('value')
    .then((snapshot) => {
      if (snapshot.exists()) {
        snapshot.forEach((productoSnapshot) => {
          const productoKey = productoSnapshot.key;
          productosRef.child(productoKey).update({ status: 'Deshabilitado' })
            .then(() => {
              alert('Producto deshabilitado correctamente.');
            })
            .catch((error) => {
              alert('Error al deshabilitar el producto: ' + error.message);
            });
        });
      } else {
        alert('Producto no encontrado.');
      }
    })
    .catch((error) => {
      console.error('Error al buscar el producto:', error);
    });
}
}


// Función para actualizar un producto por su código
function actualizarProducto() {
const codigo = prompt('Ingrese el código del producto que desea actualizar:');
if (codigo) {
  const fileInput = document.createElement('input');
  fileInput.type = 'file';
  fileInput.accept = 'image/*'; // Aceptar solo archivos de imagen

  fileInput.onchange = (event) => {
    const file = event.target.files[0];
    if (file) {
      const storageRef = storage.ref(`imagenes/${file.name}`);
      storageRef.put(file).then(() => {
        // Obtener la URL de la imagen subida
        storageRef.getDownloadURL().then((url) => {
          const nuevoNombre = prompt('Ingrese el nuevo nombre del producto:');
          const nuevoPrecio = prompt('Ingrese el nuevo precio del producto:');

          // Actualizar el producto en la base de datos con el nuevo nombre, precio y URL de la imagen
          productosRef.orderByChild('codigo').equalTo(codigo).once('value')
            .then((snapshot) => {
              if (snapshot.exists()) {
                snapshot.forEach((productoSnapshot) => {
                  const productoKey = productoSnapshot.key;
                  productosRef.child(productoKey).update({
                    nombre: nuevoNombre,
                    precio: parseFloat(nuevoPrecio),
                    url: url // Utilizar la URL de la imagen subida
                  }).then(() => {
                    alert('Producto actualizado correctamente.');
                  }).catch((error) => {
                    alert('Error al actualizar el producto: ' + error.message);
                  });
                });
              } else {
                alert('Producto no encontrado.');
              }
            })
            .catch((error) => {
              console.error('Error al buscar el producto:', error);
            });
        }).catch((error) => {
          alert('Error al obtener la URL de descarga de la imagen: ' + error.message);
        });
      }).catch((error) => {
        alert('Error al subir la imagen: ' + error.message);
      });
    }
  };

  // Hacer clic en el elemento de entrada de archivo para permitir al usuario seleccionar una imagen
  fileInput.click();
}
}

// Función para limpiar los campos de entrada y deshabilitar los botones
function limpiarCampos() {
document.getElementById('codigo').value = '';
document.getElementById('nombre').value = '';
document.getElementById('precio').value = '';
document.getElementById('status').value = '';
document.getElementById('url').value = '';

}

